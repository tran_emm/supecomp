#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "elang.h"
#include "cfg.h"


void print_mapping(FILE* f, list* map){
  while(map){
    pair* elt = map->elt;
    fprintf(f, "n%d: ", (int)(unsigned long)elt->fst);
    print_string_list(f, (list*)elt->snd);
    fprintf(f, "\n");
    map = map->next;
  }
}

list* expr_used_vars(struct expression* e){
  switch (e->etype){
    case EINT:
      return NULL;
    case EVAR:
      return list_append_string(NULL, e->var.s);
    case EUNOP:
      return expr_used_vars(e->unop.e);
    case EBINOP:
      return concat(expr_used_vars(e->binop.e1), expr_used_vars(e->binop.e2));
    default:
      printf("expr_used_vars : Unrecognized expression type : %d\n", e->etype);
  }
 return NULL;
}


list* live_after(node_t* n, list* map){
  list* live_aft = NULL;
  list** list_ptr = NULL;
  list* successors = succs_node(n);
  list* res = map;
  unsigned long int current_id;
  while(successors){
    current_id = (unsigned long int) successors->elt;
    list_ptr = ((list**) assoc_get(res, (void*)(unsigned long int) current_id));
    if(list_ptr) {
      live_aft = concat(live_aft, string_list_copy(*list_ptr));
    }
    successors = successors->next;
  }
  return live_aft;
}

list* live_before(list* live_aft, node_t* n){
  list * live_bef = NULL;
  list * used_vars = NULL;
  switch (n->type){
    case NODE_ASSIGN:
      // Liste des éléments utilisés dans le noeud
      used_vars = expr_used_vars(n->assign.e);
      // Ecriture sur var (mort de la variable)
      used_vars = list_remove_string(used_vars, n->assign.var);
      /* if (list_in_string(live_aft, n->assign.var)){ */
      live_bef = concat(live_aft, used_vars);
      /* } */
      live_bef = clear_dup_string(live_bef);
      break;
    case NODE_PRINT:
      // Liste des éléments utilisés dans le noeud
      used_vars = expr_used_vars(n->print.e);
      // Ajouter toutes les variables utilisées
      live_bef = concat(live_aft, used_vars);
      live_bef = clear_dup_string(live_bef);
      break;
    case NODE_RETURN:
      // Liste des éléments utilisés dans le noeud
      used_vars = expr_used_vars(n->ret.e);
      // Ajouter toutes les variables utilisées
      live_bef = concat(string_list_copy(live_aft), used_vars);
      live_bef = clear_dup_string(live_bef);
      break;
    case NODE_COND:
      // Liste des éléments utilisés dans le noeud
      used_vars = expr_used_vars(n->cond.cond);
      // Ajouter toutes les variables utilisées
      live_bef = concat(string_list_copy(live_aft), used_vars);
      live_bef = clear_dup_string(live_bef);
      break;
    case NODE_GOTO:
      break;
    default:
      printf("live_before : Node type not found : %d\n", n->type);
      exit(1);
  }
  return live_bef;
}

int new_changes;

list* liveness_graph(list* map, struct cfg* c){
  new_changes = 0;
  list* res = map;
  list** list_ptr = NULL;
  list* cur_node_vars = NULL;
  struct cfg * current_node = c;
  while (current_node){
    list* live_aft = live_after(current_node->node, res);
    /* print_string_list(stdout, live_aft); */
    /* printf(" %i \n", current_node->id); */
    list* live_bef = live_before(live_aft, current_node->node);
    /* print_string_list(stdout, live_bef); */
    /* printf(" %i \n", current_node->id); */

    list_ptr = ((list**) assoc_get(res, (void *)(unsigned long int)(current_node->id)));
    if (list_ptr){
      cur_node_vars = *list_ptr;
    } /* else { printf("empty list_ptr\n"); } */
    if(!list_string_incl(live_bef, cur_node_vars)){
      /* printf("Yolo\n"); */
      new_changes = 1;
      res = assoc_set(res, (void*)(unsigned long int)(current_node->id), live_bef);
    }
    cur_node_vars = NULL;
    current_node = current_node->next;
    /* print_mapping(stdout, map); */
    /* printf("----------------------- \n"); */
  }
  return res;
}

list* liveness_prog(struct cfg_prog* p){
  list* map = NULL;
  do {
    map = liveness_graph(map, p->graph);
    /* print_mapping(stdout, map); */
    /* printf("END ----------------------- \n"); */
  } while (new_changes);
  return map;
}
