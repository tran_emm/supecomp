#include "datatypes.h"
#include "elang.h"
#include "elang_print.h"
#include "cfg.h"
#include "cfg_liveness.h"
#include "cfg_gen.h"
#include "globals.h"

int num_changed = 0;
int roundnr = 0;

void dead_assign_elimination_graph(list* live, cfg* c){
  num_changed =0;
  while (c){
    if (c->node->type == NODE_ASSIGN ){
      list ** list_ptr = assoc_get(live, (void*)(unsigned long int)c->id);
      list * live_vars = NULL;
      if (list_ptr) {
        /* printf("dead_assign_elimination_graph : \n"); */
        /* print_mapping(stdout, *list_ptr); */
        live_vars = *list_ptr;
      } //else {
        /* printf("dead_assgin_elimination_graph : node id not found : %i \n", c->id); */
      /* } */
      if (!(list_in_string(live_vars, c->node->assign.var))){
        /* printf("Replacing by goto ! \n"); */
          num_changed++;
          c->node->type = NODE_GOTO;
          c->node->goto_succ = c->node->assign.succ;
        }
    }
    c = c->next;
  }
}

cfg_prog* dead_assign_elimination_prog(cfg_prog* c){
  list * live = NULL;
  do {
    live = liveness_prog(c);
    /* printf("round nb : %i\n", roundnr++); */
    dead_assign_elimination_graph(live, c->graph);
  } while (num_changed);
  return c;
}
