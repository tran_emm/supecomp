#include <stdlib.h>
#include <string.h>
#include "elang.h"
#include "elang_run.h"
#include "cfg.h"
#include "cfg_value_analysis.h"


void print_aval(FILE* f, aval* av){
  switch(av->type){
  case TOP: fprintf(f, "top"); break;
  case BOT: fprintf(f, "bot"); break;
  case CST: fprintf(f, "%d", av->cst); break;
  default: fprintf(f, "???"); break;
  }
}

void print_astate(FILE* f, astate* as){
  while(as){
    fprintf(f, "%s: ", as->var);
    print_aval(f, as->av);
    as = as->next;
    if(as) fprintf(f, ", ");
  }
}

void print_value_analysis_result(FILE* f, list* cfg_astate){
  while(cfg_astate){
    pair* p = cfg_astate->elt;
    int nid = (unsigned long)p->fst;
    astate* as = (astate*)p->snd;
    fprintf(f, "n%d: ", nid);
    print_astate(f, as);
    fprintf(f, "\n");
    cfg_astate = cfg_astate->next;
  }
}

aval* mktop(){
  aval* res = malloc(sizeof(aval));
  res->type = TOP;
  return res;
}

aval* mkbot(){
  aval* res = malloc(sizeof(aval));
  res->type = BOT;
  return res;
}

aval* mkcst(int c){
  aval* res = malloc(sizeof(aval));
  res->type = CST;
  res->cst = c;
  return res;
}

aval* aval_copy(aval* a){
  aval* res = malloc(sizeof(aval));
  res->type = a->type;
  res->cst = a->cst;
  return res;
}

aval* aunop(unop_t u, aval* a){
  switch(a->type){
  case BOT: return mkbot();
  case TOP: return mktop();
  case CST: return mkcst(run_unop(u, a->cst));
  }
  printf("aunop : aval type not matched !\n");
  exit(1);
}

aval* abinop(binop_t b, aval* a1, aval* a2){
  // Cas particulier
  if (b == EMUL && (a1->type == CST && a1->cst == 0)) return mkcst(0);
  if (b == EMUL && (a2->type == CST && a2->cst == 0)) return mkcst(0);
  if (b == EMOD && a2->type == CST && a2->cst == 1) return mkcst(0);

  switch(a1->type){
  case BOT: return mkbot();
  case TOP: return mktop();
  case CST:
    switch(a2->type){
    case BOT: return mkbot();
    case TOP: return mktop();
    case CST: return mkcst(run_binop(b, a1->cst, a2->cst));
    }
  }
  printf("abinop : aval type not matched !\n");
  exit(1);
}

aval* astate_lookup(astate* s, char* var){
  if(!s){
    return mkbot();
  }
  if(!strcmp(s->var, var)) return s->av;
  return astate_lookup(s->next, var);
}

aval* aexpr(astate* s, expression* e){
  switch(e->etype){
    case EINT:
      return mkcst(e->eint.i);
    case EVAR:
      return aval_copy(astate_lookup(s, e->var.s));
    case EUNOP:
      return aval_copy(aunop(e->unop.unop, aexpr(s, e->unop.e)));
    case EBINOP:
      return aval_copy(abinop(e->binop.binop, aexpr(s, e->binop.e1), aexpr(s, e->binop.e2)));
  }
  printf("aexpr : expression type not found : %i\n", e->etype);
  exit(1);
}

list* preds_aux(cfg* c, int node_id, list* aux){
  if(!c) return aux;
  if(list_in_int(succs_node(c->node), node_id))
    return preds_aux(c->next, node_id, cons_int(c->id, aux));
  return preds_aux(c->next, node_id, aux);
}

list* preds(cfg* c, int node_id){
  return preds_aux(c, node_id, NULL);
}

astate* astate_set(astate* s, char* var, aval* av){
  if(!s){
    astate* as = malloc(sizeof(astate));
    as->next = NULL;
    as->var = var;
    as->av = av;
    return as;
  }
  //  printf("astate_set (*s)->var = '%s'\n", s->var);
  if(!strcmp(s->var, var)){
    s->av = av;
    return s;
  } else {
    s->next = astate_set(s->next, var, av);
    return s;
  }
}

aval* meet_aval(aval* a1, aval* a2){
  switch (a1->type){
    case BOT: return aval_copy(a2);
    case TOP: return mktop();
    case CST:
    switch (a2->type){
      case BOT: return aval_copy(a1);
      case TOP: return mktop();
      case CST: if (a1->cst == a2->cst) return mkcst(a1->cst); else return mktop();
    }
  }
  printf("meet_aval : aval type not matched !\n");
  exit(1);
}

astate* meet(astate* a1, astate* a2){
  astate* res = NULL;
  while (a1){
    res = astate_set(res, a1->var, a1->av);
    a1 = a1->next;
  }
  while (a2){
    res = astate_set(res, a2->var, meet_aval(a2->av, astate_lookup(res, a2->var)));
    a2 = a2->next;
  }
  return res;
}

astate* astate_before(cfg_prog* p, int node_id, list* cfg_astate){
  astate* res = NULL;
  list * cur_preds = preds(p->graph, node_id);
  if (node_id == p->entry){
    list * args = p->args;
    while (args){
      res = astate_set(res, args->elt, mktop());
      args = args->next;
    }
    return res;
  }
  while (cur_preds){
    list* list_tmp = (list*)assoc_get(cfg_astate, (void*)(unsigned long int)cur_preds->elt);
    if (list_tmp){
      res = meet(res, list_tmp->elt);
    } //else printf("\nNot found : %li\n", (unsigned long int)cur_preds->elt);

    cur_preds = cur_preds->next;
  }
  /* printf("-------------------END-----------------------\n"); */
  return res;
}

int more_precise_aval(aval* a1, aval* a2){
  return (a1->type != a2->type);
}

int more_precise(astate* a1, astate* a2){
  if(!a1) return 0;
  aval* av2 = astate_lookup(a2, a1->var);
  if(more_precise_aval(a1->av, av2)) return 1;
  return more_precise(a1->next, a2);
}

int num_changes = 0;

    /* pair* p = cfg_astate->elt; */
    /* int nid = (unsigned long)p->fst; */
    /* astate* as = (astate*)p->snd; */
    /* cfg_astate = cfg_astate->next; */



list* acfg(list* cfg_astate, cfg_prog* p){
  num_changes = 0;
  cfg* graph = p->graph;
  astate* astate_bef_cur = NULL;
  while (graph){
    astate_bef_cur = astate_before(p, graph->id, cfg_astate);
    switch (graph->node->type){
      case NODE_ASSIGN:
        astate_bef_cur = astate_set(astate_bef_cur,
                                    graph->node->assign.var,
                                    aexpr(astate_bef_cur, graph->node->assign.e)
        );
        break;
      case NODE_PRINT:
        break;
      case NODE_RETURN:
        break;
      case NODE_COND:
        break;
      case NODE_GOTO:
        break;
      default:
        printf("acfg : node type not recognized %i\n", graph->node->type);
    }
    // En cas de changements, mettre à jour le flag global et l'état
    list* list_tmp = ((list*)assoc_get(cfg_astate, (void*)(unsigned long)graph->id));
    if (!list_tmp || (list_tmp && more_precise(astate_bef_cur, list_tmp->elt))){
      num_changes++;
      /* printf("Adding : "); */
      /* print_astate(stdout, astate_bef_cur); */
      /* printf("\n"); */
      cfg_astate = assoc_set(cfg_astate, (void*)(unsigned long)graph->id, astate_bef_cur);
      // Fix immonde
      /* cfg_astate = cfg_astate->next; */
      /* printf("Printing cfg :"); */
      /* print_value_analysis_result(stdout, cfg_astate); */
      /* printf("\n"); */
    } //else {
    /*   printf("\nacfg : Not found : %li\n", (unsigned long int)graph->id); */
    /* } */
    graph = graph->next;
  }
  return cfg_astate;
}

list* value_analysis_cfg_prog(cfg_prog* p){
  list* cfg_astate = NULL;
  /* Initialisation de l'état abstrait (pas obligatoire, cependant rassurant) */
  /* while (graph){ */
  /*   cfg_astate = assoc_set(cfg_astate, (void*)(unsigned long) graph->id, NULL); */
  /*   graph = graph->next; */
  /* } */
  do {
    cfg_astate = acfg(cfg_astate, p);
    /* print_value_analysis_result(stdout, cfg_astate); */
  } while (num_changes);
  /* printf("--------------------------- value_analysis_cfg_prog result ---------------- : \n"); */
  /* print_value_analysis_result(stdout, cfg_astate); */
  /* printf("\n"); */
  return cfg_astate;
}
