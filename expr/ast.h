#ifndef AST_H
#define AST_H


enum ast_tag {
              AST_STRING,              //0
              AST_NODE_FUNDEF,         //1
              AST_NODE_FUNARGS,        //2
              AST_NODE_FUNBODY,        //3
              AST_INT,                 //4
              AST_EADD,                //5
              AST_ESUB,
              AST_EMOD,
              AST_EMUL,
              AST_EDIV,
              AST_ENEG,                //10
              AST_ENOT,
              AST_CLT,
              AST_CLE,
              AST_CGT,
              AST_CGE,                 //15
              AST_CEQ,
              AST_CNEQ,

              AST_IASSIGN,
              AST_IBLOCK,
              AST_IIFTHENELSE,         //20
              AST_IWHILE,
              AST_IRETURN,
              AST_IPRINT,

              AST_EQS,
              AST_CMPS,                //25
              AST_TERMS,
              AST_FACTORS              //27
};

struct ast_node {
  enum ast_tag tag;
  union {
    struct list* children;
    char* string;
    int integer;
  };
};

struct ast_node* make_node(enum ast_tag, struct list*);
struct ast_node* make_string_leaf(char*);
struct ast_node* make_int_leaf(int);


char* string_of_string_leaf(struct ast_node* ast);
int int_of_int_leaf(struct ast_node* ast);

void free_ast(struct ast_node*);

#endif
