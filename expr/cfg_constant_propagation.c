#include <assert.h>
#include <stdlib.h>
#include "elang.h"
#include "elang_gen.h"
#include "elang_run.h"
#include "cfg.h"
#include "cfg_gen.h"
#include "cfg_value_analysis.h"


expression* asimpl(astate* as, expression* e){
  aval * var = NULL;
  expression * e1 = NULL;
  expression * e2 = NULL;
  switch (e->etype){
    case EINT:
      return e;
    case EVAR:
      var = astate_lookup(as, e->var.s);
      if (var->type == CST){
        /* printf("Replaced : %s\n", e->var.s); */
        e->etype = EINT;
        e->eint.i = var->cst;
      } //else {
      /*   printf("Not Replaced : %s\n", e->var.s); */
      /* } */
      return e;
    case EUNOP:
      e1 = asimpl(as, e->unop.e);
      if (e1->etype == EINT){
        e->eint.i = run_unop(e->unop.unop, e1->eint.i);
        e->etype=EINT;
      } else {
        e->unop.e = e1;
      }
      return e;
    case EBINOP:
      e1 = asimpl(as, e->binop.e1);
      e2 = asimpl(as, e->binop.e2);
      if (e1->etype == EINT && e2->etype == EINT){
        e->eint.i = run_binop(e->binop.binop, e1->eint.i, e2->eint.i);
        e->etype=EINT;
      } else {
        e->binop.e1 = e1;
        e->binop.e2 = e2;
      }
      return e;
    default :
      printf("aexpr : expression type not found : %i\n", e->etype);
      exit(1);
  }
}

void constant_propagation_graph(list* value_analysis, cfg* c, cfg_prog* p){
  if (c){
    astate * cur_as = astate_before(p, c->id, value_analysis);
    switch (c->node->type){
      case NODE_ASSIGN:
        c->node->assign.e = asimpl(cur_as, c->node->assign.e);
        break;
      case NODE_PRINT:
        c->node->print.e = asimpl(cur_as, c->node->print.e);
        break;
      case NODE_RETURN:
        c->node->ret.e = asimpl(cur_as, c->node->ret.e);
        break;
      case NODE_COND:
        c->node->cond.cond = asimpl(cur_as, c->node->cond.cond);
        if (c->node->cond.cond->etype == EINT){
          c->node->type = NODE_GOTO;
          c->node->goto_succ = c->node->cond.cond->eint.i
            ? c->node->cond.succ1
            : c->node->cond.succ2;
        }
        break;
      case NODE_GOTO:
        break;
      default:
        printf("constant_propagation_graph : Node type not recognized : %i\n", c->node->type);
        exit(1);
    }
    constant_propagation_graph(value_analysis, c->next, p);
  }
}

cfg_prog* constant_propagation_cfg_prog(cfg_prog* cfg){

  list* value_analysis = value_analysis_cfg_prog(cfg);
  /* printf("----------------------------------------\n"); */
  /* print_value_analysis_result(stdout, value_analysis); */

  constant_propagation_graph(value_analysis, cfg->graph, cfg);

  cfg->graph = clean_cfg(cfg->graph, cfg->entry);
  return cfg;
}
