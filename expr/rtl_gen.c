#include <stdlib.h>
#include <string.h>
#include "elang.h"
#include "cfg.h"
#include "rtl.h"

rtl_op* new_rtl_op_mov(int rd, int rs){
  rtl_op* op = malloc(sizeof(rtl_op));
  op->type = RMOV;
  op->mov.rd = rd;
  op->mov.rs = rs;
  return op;
}

rtl_op* new_rtl_op_label(int label){
  rtl_op* op = malloc(sizeof(rtl_op));
  op->type = RLABEL;
  op->label.lab = label;
  return op;
}

rtl_op* new_rtl_op_print(int rs){
  rtl_op* op = malloc(sizeof(rtl_op));
  op->type = RPRINT;
  op->print.rs = rs;
  return op;
}

rtl_op* new_rtl_op_return(int rs){
  rtl_op* op = malloc(sizeof(rtl_op));
  op->type = RRET;
  op->ret.rs = rs;
  return op;
}

rtl_op* new_rtl_op_goto(int succ){
  rtl_op* op = malloc(sizeof(rtl_op));
  op->type = RGOTO;
  op->rgoto.succ = succ;
  return op;
}

rtl_op* new_rtl_op_branch(int rs, int succtrue, int succfalse){
  rtl_op* op = malloc(sizeof(rtl_op));
  op->type = RBRANCH;
  op->branch.rs = rs;
  op->branch.succtrue = succtrue;
  op->branch.succfalse = succfalse;
  return op;
}

rtl_op* new_rtl_op_binop(binop_t b, int rd, int rs1, int rs2){
  rtl_op* op = malloc(sizeof(rtl_op));
  op->type = RBINOP;
  op->binop.binop = b;
  op->binop.rd = rd;
  op->binop.rs1 = rs1;
  op->binop.rs2 = rs2;
  return op;
}

rtl_op* new_rtl_op_unop(unop_t u, int rd, int rs){
  rtl_op* op = malloc(sizeof(rtl_op));
  op->type = RUNOP;
  op->unop.unop = u;
  op->unop.rd = rd;
  op->unop.rs = rs;
  return op;
}

rtl_op* new_rtl_op_imm(int rd, int imm){
  rtl_op* op = malloc(sizeof(rtl_op));
  op->type = RIMM;
  op->imm.rd = rd;
  op->imm.imm = imm;
  return op;
}

typedef struct symtable {
  struct symtable* next;
  char* var;
  int reg;
} symtable;

void free_symtable(symtable* s){
  if(s){
    free(s->var);
    free_symtable(s->next);
    free(s);
  }
}

symtable* symt = NULL;

int curreg = 0;

int new_reg(){
  return curreg++;
}

int get_reg_for_var(char* v){
  symtable* cur = symt;
  symtable* prec = NULL;
  while(cur){
    if (!(strcmp(cur->var, v))) return cur->reg;
    prec = cur;
    cur = cur->next;
  }
  // No registers was found, creating a new one
  int new_id = new_reg();
  cur = malloc(sizeof(symtable));
  cur->var = v;
  cur->reg = new_id;
  cur->next = NULL;

  if (prec) prec->next = cur;
  else symt = cur;
  return new_id;
}

typedef struct expr_compiled {
  int r;
  list* ops;
} expr_compiled;

expr_compiled* new_expr_compiled(){
  expr_compiled* ec = (expr_compiled*)malloc(sizeof(expr_compiled));
  ec->ops = NULL;
  return ec;
}

expr_compiled* rtl_ops_of_expression(struct expression* e){
  expr_compiled* res = new_expr_compiled();
  expr_compiled* tmp1 = NULL;
  expr_compiled* tmp2 = NULL;
  switch (e->etype){
    case EINT:
      res->r = new_reg();
      res->ops = list_append(res->ops, new_rtl_op_imm(res->r, e->eint.i));
      break;
    case EVAR:
      res->r = get_reg_for_var(e->var.s);
      res->ops = NULL;
      break;
    case EUNOP:
      res->r = new_reg();
      tmp1 = rtl_ops_of_expression(e->unop.e);
      res->ops = concat(res->ops, tmp1->ops);
      res->ops = list_append(res->ops, new_rtl_op_unop(e->unop.unop, res->r, tmp1->r));
      break;
    case EBINOP:
      res->r = new_reg();
      tmp1 = rtl_ops_of_expression(e->binop.e1);
      tmp2 = rtl_ops_of_expression(e->binop.e2);
      res->ops = concat(tmp1->ops, tmp2->ops);
      res->ops = list_append(res->ops, new_rtl_op_binop(e->binop.binop, res->r, tmp1->r, tmp2->r));
      break;
    default:
      printf("rtl_ops_of_expression : exp type not found : %d\n", e->etype);
      exit(1);
  }
  return res;
}

list* rtl_ops_of_cfg_node(node_t* c){
  list* res = NULL;
  expr_compiled* tmp = NULL;
  switch(c->type){
    case NODE_ASSIGN:
      tmp = rtl_ops_of_expression(c->assign.e);
      res = tmp->ops;
      res = list_append(res, new_rtl_op_mov(get_reg_for_var(c->assign.var), tmp->r));
      res = list_append(res, new_rtl_op_goto(c->assign.succ));
      break;
    case NODE_PRINT:
      tmp = rtl_ops_of_expression(c->print.e);
      res = tmp->ops;
      res = list_append(res, new_rtl_op_print(tmp->r));
      res = list_append(res, new_rtl_op_goto(c->print.succ));
      break;
    case NODE_RETURN:
      tmp = rtl_ops_of_expression(c->ret.e);
      res = tmp->ops;
      res = list_append(res, new_rtl_op_return(tmp->r));
      break;
    case NODE_COND:
      tmp = rtl_ops_of_expression(c->cond.cond);
      res = tmp->ops;
      res = list_append(res, new_rtl_op_branch(tmp->r, c->cond.succ1, c->cond.succ2));
      break;
    case NODE_GOTO:
      res = list_append(res, new_rtl_op_goto(c->goto_succ));
      break;
    default :
      printf("rtl_ops_of_cfg_node : node type not found : %d\n", c->type);
      exit(1);
  }
  return res;
}

rtl* rtl_of_cfg_graph(cfg* c){
  rtl* res = NULL;
  if (!c) return NULL;
  rtl* cur = malloc(sizeof(struct rtl));
  cur->ops = rtl_ops_of_cfg_node(c->node);
  cur->id = c->id;
  cur->next = NULL;
  res = cur;
  c = c->next;
  while(c){
    cur->next = malloc(sizeof(struct rtl));
    cur = cur->next;
    cur->ops = rtl_ops_of_cfg_node(c->node);
    cur->id = c->id;
    cur->next = NULL;

    c = c->next;
  }
  return res;
}

rtl_prog* rtl_of_cfg_prog(cfg_prog* cfg){
  struct rtl_prog* rtl = (struct rtl_prog*)malloc(sizeof(struct rtl_prog));
  rtl->fname = strdup(cfg->fname);
  rtl->args = NULL;
  list * cfg_args = cfg->args;
  while (cfg_args){
    rtl->args = list_append(rtl->args, (void*)(unsigned long int) get_reg_for_var((char*)cfg_args->elt));
    cfg_args = cfg_args->next;
  }
  rtl->graph = rtl_of_cfg_graph(cfg->graph);
  rtl->entry = cfg->entry;
  /* free_symtable(symt); */
  return rtl;
}


void free_rtl_op(rtl_op* n){
  if(!n) return;
  free(n);
}

void free_rtl_ops(list* l){
  if(!l) return;
  free_rtl_ops(l->next);
  free_rtl_op(l->elt);
}

void free_rtl_graph(rtl* r){
  if(r){
    free_rtl_graph(r->next);
    free_rtl_ops(r->ops);
    //free_list(r->ops);
    free(r);
  }
}

void free_rtl(rtl_prog* rtl){
  if(rtl){
    free(rtl->fname);
    free_list(rtl->args);
    free_rtl_graph(rtl->graph);
    free(rtl);
  }
}
